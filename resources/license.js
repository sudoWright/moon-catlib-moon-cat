// copyright ©2021 ponderware
// licensed under the AGPL v3, see https://www.gnu.org/licenses/agpl-3.0.en.html

// The above copyright and licensing exclude the embedded nodeca/pako code which is licensed under a combination of MIT & ZLIB licenses. See https://github.com/nodeca/pako
