(ns libmooncat.ethereum.eth-util
  (:require
   [clojure.string :as str]
   [libmooncat.util :as util])
  #?(:clj
     (:import
      [org.web3j.utils Numeric])))


(def zeros64 (str/join (repeat 64 "0")))

(defn left-pad-hex-to-32-bytes [hex]
  (str (subs zeros64 (count hex)) hex))

(defn right-pad-hex-to-32-bytes [hex]
  (str hex (subs zeros64 (count hex))))

(defn left-pad-hex [hex byte-count]
  (let [zeros (subs zeros64 (- 64 (* 2 byte-count)))]
    (str (subs zeros (count hex)) hex)))

(defn number->hex [x]
  (let [s (str x)]
    #?(:clj (.toString (BigInteger. s) 16)
       :cljs (.toString (js/BigInt s) 16))))

(defn encode-uint [bit-count x]
  (let [byte-count (quot bit-count 8)]
    (left-pad-hex (number->hex x) byte-count)))

(defn extract-uint256 [hex-word]
  (str
   #?(:clj (BigInteger. hex-word 16)
      :cljs (js/BigInt (str "0x" hex-word)))))

(def wei-zeros "000000000000000000")

(defn trim-trailing-zeros [s]
  (str/replace s #"0+$" ""))

(defn trim-leading-zeros [s]
  (str/replace s #"^0+" ""))

(defn convert-wei-to-eth-str [wei]
  (let [wei (str #?(:clj (BigInteger. wei)
                    :cljs (js/BigInt wei)))
        wei-string-length (count wei)]
    (cond
      (= wei "0") "0"
      (< wei-string-length 18) (trim-trailing-zeros (str "0." (subs wei-zeros wei-string-length) wei))
      (= wei-string-length 18) (trim-trailing-zeros (str "0." wei))
      :else (let [eth (subs wei 0 (- wei-string-length 18))
                  decimal (subs wei (- wei-string-length 18))]
              (if (= decimal wei-zeros)
                eth
                (str eth "." (trim-trailing-zeros decimal)))))))

(defn convert-eth-to-wei-str [eth]
  (let [eth (trim-leading-zeros eth)]
    (cond
      (str/blank? eth) "0"
      (nil? (re-find #"\." eth)) (str eth wei-zeros)
      :else (let [[eth decimal] (str/split eth #"\.")
                  decimal-length (count decimal)
                  decimal (str decimal (subs wei-zeros decimal-length))
                  res (trim-leading-zeros (str eth decimal))]
              (if (str/blank? res)
                "0"
                res)))))

(defn extract-uint-small [hex-word]
  #?(:clj (Long/parseLong hex-word 16)
     :cljs (js/parseInt hex-word 16)))

(defn extract-bytes5 [hex-word]
  (str "0x" (subs hex-word 0 10)))

(defn extract-address [hex-word]
  (str "0x" (subs hex-word 24)))

(defn extract-boolean [hex-word]
  (= "1" (subs hex-word 63 64)))

(defn extract-bytes-to-string [hex-word]
  (when-not (re-matches #"^0+$" hex-word)
    #?(:clj (try
              (String. (Numeric/hexStringToByteArray (str/replace hex-word #"(00)+$" "")) "UTF-8")
              (catch Exception e
                "INVALID UNICODE NAME"))

       :cljs (try
               (let [s (str/replace hex-word #"(00)+$" "")
                     ba (util/hex->byte-array s)]
                 (.decode (new js/TextDecoder) ba))
               (catch :default e
                 "FAILED TO PARSE UNICODE")))))

(defn get-word [hex-word word-index]
  (let [i (if (str/starts-with? hex-word "0x")
            2
            0)
        i (+ i (* 64 word-index))]
    (subs hex-word i (+ 64 i))))

(defn get-words [hex-word word-index word-count]
  (let [i (if (str/starts-with? hex-word "0x")
            2
            0)
        i (+ i (* 64 word-index))]
    (subs hex-word i (+ (* word-count 64) i))))

(defn extract-positions [hex-words]
  (let [p0-x (extract-uint-small (subs hex-words 0 2))
        p0-y (extract-uint-small (subs hex-words 2 4))

        p1-x (extract-uint-small (subs hex-words 64 66))
        p1-y (extract-uint-small (subs hex-words 66 68))

        p2-x (extract-uint-small (subs hex-words 128 130))
        p2-y (extract-uint-small (subs hex-words 130 132))

        p3-x (extract-uint-small (subs hex-words 192 194))
        p3-y (extract-uint-small (subs hex-words 194 196))]
    [[p0-x p0-y]
     [p1-x p1-y]
     [p2-x p2-y]
     [p3-x p3-y]]))

(defn extract-palette [hex-word]
  (let [i0 (extract-uint-small (subs hex-word 0 2))
        i1 (extract-uint-small (subs hex-word 2 4))
        i2 (extract-uint-small (subs hex-word 4 6))
        i3 (extract-uint-small (subs hex-word 6 8))
        i4 (extract-uint-small (subs hex-word 8 10))
        i5 (extract-uint-small (subs hex-word 10 12))
        i6 (extract-uint-small (subs hex-word 12 14))
        i7 (extract-uint-small (subs hex-word 14 16))]
    [i0 i1 i2 i3 i4 i5 i6 i7]))

(defn extract-all-palettes [hex-words]
  (loop [i 0
         palettes []]
    (if (== i 7)
      palettes
      (recur (inc i) (conj palettes (extract-palette (subs hex-words (* i 64) (* (inc i) 64))))))))

(defn extract-string [data word-index]
  (let [data (if (str/starts-with? data "0x")
               (subs data 2)
               data)
        string-loc (extract-uint-small (get-word data word-index))
        string-len (extract-uint-small (get-word data (quot string-loc 32)))
        string-bytes (subs data (+ (* 2 string-loc) 64) (+ (* 2 string-loc) 64 (* 2 string-len)))]
    (extract-bytes-to-string string-bytes)))
