(ns libmooncat.ethereum.rpc
  (:require
   [clojure.string :as str]
   [libmooncat.data.contracts :as contracts]
   [libmooncat.data.resources :as resources]
   [libmooncat.ethereum.eth-util :as eth-util]
   [libmooncat.util :as util]
   #?(:clj [libmooncat.ethereum.macros :refer [contract-function-data-builder function-selector with-result-data]]
      :cljs [libmooncat.ethereum.macros :refer [contract-function-data-builder function-selector]])
   #?(:cljs [libmooncat.ethereum.macros-cljs :refer [with-result-data]])
   [libmooncat.ethereum.http :refer [make-contract-request]]))

(defn- add-acclimator-details [rpc-url {:as result :keys [rescue-order]}]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncatacclimator)
                                                 ((contract-function-data-builder "ownerOf" [:uint256 token-id])
                                                  {:token-id rescue-order}))]
    (let [true-owner (eth-util/extract-address (eth-util/get-word data 0))]
      (assoc result
             :owner true-owner
             :is-acclimated true
             :contract {:token-id rescue-order
                        :description "MoonCatAcclimator"
                        :address (:address contracts/mooncatacclimator)
                        :capabilities ["ERC20" "ERC721" "ERC998"]}))))


(defn get-cat-contract-details [rpc-url cat-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncatrescue)
                                                 ((contract-function-data-builder "getCatDetails" [:bytes5 cat-id])
                                                  {:cat-id cat-id}))]
    (let [owner (eth-util/extract-address (eth-util/get-word data 1))
          result {:cat-id (eth-util/extract-bytes5 (eth-util/get-word data 0))
                  :rescue-order (resources/cat-id->rescue-order cat-id)
                  :is-acclimated false
                  :cat-name (eth-util/extract-bytes-to-string (eth-util/get-word data 2))}]
      (condp = owner
        (:address contracts/mooncatacclimator)
        (add-acclimator-details rpc-url result)

        (:address contracts/deprecated-unofficial-mooncat-wrapper)
        (assoc result
               :contract {:description "Unsupported Unofficial MoonCat Wrapper"
                          :address (:address contracts/deprecated-unofficial-mooncat-wrapper)
                          :warning "Unsupported Contract"})

        (assoc result
               :owner owner
               :contract {:token-id cat-id
                          :description "MoonCatRescue"
                          :address (:address contracts/mooncatrescue)
                          :capabilities ["ERC20"]})))))

(defn is-cat-acclimated [rpc-url cat-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncatrescue)
                                                 ((contract-function-data-builder "getCatDetails" [:bytes5 cat-id])
                                                  {:cat-id cat-id}))]
    (= (eth-util/extract-address (eth-util/get-word data 1))
       (:address contracts/mooncatacclimator))))

;;;; Accessories

(def local-rpc "http://localhost:8545")

(defn get-total-accessories [rpc-url]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "totalAccessories") nil))]
    (eth-util/extract-uint256 (eth-util/get-word data 0))))

(defn get-accessory-palette-count [rpc-url accessory-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "accessoryPaletteCount" [:uint256 accessory-id])
                                                  {:accessory-id accessory-id}))]
    (eth-util/extract-uint-small (eth-util/get-word data 0))))

(defn get-accessory-palette [rpc-url accessory-id palette-index]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "accessoryPalette"
                                                                                  [:uint256 accessory-id]
                                                                                  [:uint256 palette-index])
                                                  {:accessory-id accessory-id
                                                   :palette-index palette-index}))]
    (eth-util/extract-palette (eth-util/get-word data 0))))



(defn get-accessory-info [rpc-url accessory-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "accessoryInfo" [:uint256 accessory-id])
                                                  {:accessory-id accessory-id}))]
    (let [price-wei (eth-util/extract-uint256 (eth-util/get-word data 11))]
      {:total-supply (eth-util/extract-uint-small (eth-util/get-word data 0))
       :available-supply (eth-util/extract-uint-small (eth-util/get-word data 1))
       :name (eth-util/extract-bytes-to-string (eth-util/get-word data 2))
       :manager (eth-util/extract-address (eth-util/get-word data 3))
       :meta (eth-util/extract-uint-small (eth-util/get-word data 4))
       :available-palettes (eth-util/extract-uint-small (eth-util/get-word data 5))
       :positions (eth-util/extract-positions (eth-util/get-words data 6 4))
       :available (eth-util/extract-boolean (eth-util/get-word data 10))
       :price-wei price-wei
       :price-eth (eth-util/convert-wei-to-eth-str price-wei)
     })))


(defn get-accessory-image-data [rpc-url accessory-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "accessoryImageData" [:uint256 accessory-id])
                                                  {:accessory-id accessory-id}))]
    (let [idat-str-offset (+ 2 64 (* 2 (eth-util/extract-uint-small (eth-util/get-word data 14))))
          idat-str-length (* 2 (eth-util/extract-uint-small (eth-util/get-word data 15)))]
      {:positions (eth-util/extract-positions (eth-util/get-words data 0 4))
       :palettes (eth-util/extract-all-palettes (eth-util/get-words data 4 7))
       :width (eth-util/extract-uint-small (eth-util/get-word data 11))
       :height (eth-util/extract-uint-small (eth-util/get-word data 12))
       :meta (eth-util/extract-uint-small (eth-util/get-word data 13))
       :idat (str "0x" (subs data idat-str-offset (+ idat-str-offset idat-str-length)))})))

(defn get-accessory-eligible-list [rpc-url accessory-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "accessoryEligibleList" [:uint256 accessory-id])
                                                  {:accessory-id accessory-id}))]
    (mapv #(str "0x" %) (re-seq #".{64}" (subs data 2)))))


(defn get-accessory [rpc-url accessory-id]
  #?(:clj (assoc
           (merge (get-accessory-info rpc-url accessory-id)
                  (get-accessory-image-data rpc-url accessory-id))
           :eligible-list (get-accessory-eligible-list rpc-url accessory-id))
     :cljs (.then
            (get-accessory-info rpc-url accessory-id)
            (fn [info]
              (.then
               (get-accessory-image-data rpc-url accessory-id)
               (fn [image-data]
                 (.then
                  (get-accessory-eligible-list rpc-url accessory-id)
                  (fn [eligible-list]
                    (new js/Promise
                         (fn [resolve reject]
                           (resolve
                            (assoc (merge info image-data) :eligible-list eligible-list))))))))))))

(defn is-accessory-unique? [rpc-url idat-hex]
  (let [idat-hex (util/clean-hex-prefix idat-hex)
        offset-hex "0000000000000000000000000000000000000000000000000000000000000020"
        len (/ (count idat-hex) 2)
        len-hex (eth-util/encode-uint 256 len)
        call-data (str "0x" (function-selector "isAccessoryUnique" "bytes") offset-hex len-hex idat-hex)]
    (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories) call-data)]
      (= "1" (subs data 65)))))

;;;; Accessories - Manager

(defn get-total-managed-accessories [rpc-url address]
  (with-result-data [acc-id (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                   ((contract-function-data-builder "balanceOf" [:address address])
                                                    {:address address}))]
    (eth-util/extract-uint-small (eth-util/get-word acc-id 0))))

(defn get-managed-accessory-id-by-index [rpc-url address index]
  (let [call-data ((contract-function-data-builder "managedAccessoryByIndex"
                                                   [:address address]
                                                   [:uint256 index])
                   {:address address
                    :index index})]
    (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories) call-data)]
      (eth-util/extract-uint-small (eth-util/get-word data 0)))))

;;;; Accessories - MoonCat

(defn get-total-mooncat-accessories [rpc-url rescue-order]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "balanceOf" [:uint256 rescue-order])
                                                  {:rescue-order rescue-order}))]
    (let [total (eth-util/extract-uint-small (eth-util/get-word data 0))]
      total)))

(defn get-mooncat-accessory [rpc-url rescue-order owned-accessory-index]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/mooncataccessories)
                                                 ((contract-function-data-builder "ownedAccessoryByIndex"
                                                                                  [:uint256 rescue-order]
                                                                                  [:uint256 owned-accessory-index])
                                                  {:rescue-order rescue-order
                                                   :owned-accessory-index owned-accessory-index}))]
      {:accessory-id (eth-util/extract-uint256 (eth-util/get-word data 0))
       :palette-index (eth-util/extract-uint-small (eth-util/get-word data 1))
       :z-index (eth-util/extract-uint-small (eth-util/get-word data 2))}))

(defn get-drawable-mooncat-accessory [rpc-url rescue-order owned-accessory-index]
  #?(:clj (let [mooncat-accessory (get-mooncat-accessory rpc-url rescue-order owned-accessory-index)
                image-data (get-accessory-image-data rpc-url (:accessory-id mooncat-accessory))]
            (merge mooncat-accessory image-data))
     :cljs (.then (get-mooncat-accessory rpc-url rescue-order owned-accessory-index)
                  (fn [mooncat-accessory]
                    (.then (get-accessory-image-data rpc-url (:accessory-id mooncat-accessory))
                           (fn [image-data]
                             (new js/Promise
                                  (fn [resolve reject]
                                    (resolve (merge mooncat-accessory image-data))))))))))


(defn get-lootprint [rpc-url lootprint-id]
  (with-result-data [data (make-contract-request rpc-url (:address contracts/lootprintsformooncats)
                                                 ((contract-function-data-builder "getDetails"
                                                                                  [:uint256 lootprint-id])
                                                  {:lootprint-id lootprint-id}))]
    {:exists (= 3 (eth-util/extract-uint-small (eth-util/get-word data 0)))
     :class (eth-util/extract-string data 1)
     :bays (eth-util/extract-uint-small (eth-util/get-word data 2))
     :color (eth-util/extract-string data 3)
     :name (eth-util/extract-string data 4)
     :owner (eth-util/extract-address (eth-util/get-word data 5))}))
