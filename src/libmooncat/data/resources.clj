(ns libmooncat.data.resources
  (:require
   [libmooncat.data.embed :refer [embed]]
   [clojure.string :as str]
   [clojure.java.io :as io]))

;;;;

(def ^:const +total-mooncats+ 25440)

(def rescue-order->cat-id (embed "mooncatdata/catIdByRescueOrder.edn"))
(def cat-id->rescue-order (reduce (fn [acc id] (assoc acc id (count acc))) {} rescue-order->cat-id))

(def rescuer-id->rescuer-address (embed "mooncatdata/rescuerAddressByRescuerId.edn"))
(def rescue-order->rescuer-id (embed "mooncatdata/rescuerIdByRescueOrder.edn"))
(defn rescue-order->rescuer [idx] (->> idx (get rescue-order->rescuer-id) (get rescuer-id->rescuer-address)))

(def groups (embed "mooncatdata/groups.edn"))
(defn get-group [group-key group-id rescue-order]
  (let [group (get-in groups [group-key group-id])]
         (if group group [rescue-order])))

(def designs (embed "mooncatdata/designs.edn"))

(def lootprints (embed "mooncatdata/lootprints.edn"))
;;;;

(defn- embed-js [resource-path]
  (str/trim (slurp (io/resource resource-path))))

(defn gen-cljs-resources []
  (str
   "(ns libmooncat.data.resources)

(def ^:const +total-mooncats+ 25440)

(def ^:const +is-limited+ false)

(def rescue-order->cat-id (mapv #(if (= 8 (count %))
                                            (str \"0x00\" %)
                                            (str \"0x\" %))
                                "(embed-js "mooncatdata/abbreviatedCatIdByRescueOrder.edn")"))
(def cat-id->rescue-order (reduce (fn [acc id] (assoc acc id (count acc))) {} rescue-order->cat-id))

(def rescuer-id->rescuer-address "(embed-js "mooncatdata/rescuerAddressByRescuerId.edn")")
(def rescue-order->rescuer-id "(embed-js "mooncatdata/rescuerIdByRescueOrder.edn")")
(defn rescue-order->rescuer [idx] (->> idx (get rescue-order->rescuer-id) (get rescuer-id->rescuer-address)))

(def groups "(embed-js "mooncatdata/groups.edn")")
(defn get-group [group-key group-id rescue-order]
  (let [group (get-in groups [group-key group-id])]
         (if group group [rescue-order])))

(def designs "(embed-js "mooncatdata/designs.edn")")

(def lootprints "(embed-js "mooncatdata/lootprints.edn")")
"))

(defn gen-limited-cljs-resources []
  (str
   "(ns libmooncat.data.resources)

(def ^:const +total-mooncats+ 25440)

(def ^:const +is-limited+ true)

(def rescue-order->cat-id (mapv #(if (= 8 (count %))
                                            (str \"0x00\" %)
                                            (str \"0x\" %))
                                "(embed-js "mooncatdata/abbreviatedCatIdByRescueOrder.edn")"))
(def cat-id->rescue-order (reduce (fn [acc id] (assoc acc id (count acc))) {} rescue-order->cat-id))

(defn rescue-order->rescuer [_])
(defn get-group [a b c] nil)

(def designs "(embed-js "mooncatdata/designs.edn")")

(def lootprints {})
"))
