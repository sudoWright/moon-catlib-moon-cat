(ns libmooncat.data.color
  (:require
   [clojure.string :as str]
   [libmooncat.data.resources :refer [rescue-order->cat-id cat-id->rescue-order]]
   #?(:clj [libmooncat.data.embed :refer [embed]])
   [libmooncat.util :as util]))

(defn get-rescue-order [cat-id]
  (cat-id->rescue-order (str/lower-case cat-id)))

(defn cat-id->glow [cat-id]
  (let [cat-id (util/clean-hex-prefix cat-id)
        r (util/hex->int (subs cat-id 4 6))
        g (util/hex->int (subs cat-id 6 8))
        b (util/hex->int (subs cat-id 8 10))]
    (if (str/starts-with? cat-id "f")
      [100 100 100]
      [r g b])))

(defn- rgb->hue [r g b]
  (let [r (/ r 255.0)
        g (/ g 255.0)
        b (/ b 255.0)
        cmax #?(:cljs (js/Math.max r g b)
                :clj (max r g b))
        cmin #?(:cljs (js/Math.min r g b)
                :clj (min r g b))
        delta (- cmax cmin)
        h
        (cond
          (== delta 0) 0
          (== r cmax) #?(:clj (* 60 (mod (/ (- g b) delta) 6))
                         :cljs (* 60 (js-mod (/ (- g b) delta) 6)))
          (== g cmax) (* 60 (+ (/ (- b r) delta) 2))
          (== b cmax) (* 60 (+ (/ (- r g) delta) 4)))]
    (if (neg? h)
      (+ h 360)
      h)))

(defn- cat-id->hue-helper [cat-id]
  (if (or (str/starts-with? cat-id "0xf") (str/starts-with? cat-id "f"))
    (let [cat-id (util/clean-hex-prefix cat-id)
          k (util/hex->int (subs cat-id 2 4))
          invert (>= k 128)]
      (if (or (and (even? k) invert) (and (odd? k) (not invert)))
        -2
        -1))
    (let [glow (cat-id->glow cat-id)]
      (rgb->hue (glow 0) (glow 1) (glow 2)))))

#?(:clj (def rescue-order->raw-hue (embed "mooncatdata/hueByRescueOrder.edn"))
   :cljs (defn rescue-order->raw-hue [rescue-order] (cat-id->hue-helper (rescue-order->cat-id rescue-order))))

(defn compat-floor
  "this keeps compatibility with the original published hue mappings when turning hues to integers"
  [x]
  (let [x (+ x 0.0000000000003)]
    (util/floor x)))

#?(:clj
   (def rescue-order->hue (mapv compat-floor (embed "mooncatdata/hueByRescueOrder.edn")))
   :cljs
   (defn rescue-order->hue [rescue-order]
     (compat-floor (cat-id->hue-helper (rescue-order->cat-id rescue-order)))))

(defn cat-id->hue [cat-id]
  #?(:clj
     (if-let [rescue-order (get-rescue-order cat-id)]
       (rescue-order->hue rescue-order)
       (compat-floor (cat-id->hue-helper cat-id)))
     :cljs (compat-floor (cat-id->hue-helper cat-id))))

(defn hue->color-key [hue-value]
  (condp >= hue-value
    -2 :white
    -1 :black
    15 :red
    45 :orange
    75 :yellow
    105 :chartreuse
    135 :green
    165 :teal
    195 :cyan
    225 :sky-blue
    255 :blue
    285 :purple
    315 :magenta
    345 :fuchsia
    :red))

(defn color-key->hue [color-key]
  (if (string? color-key)
   (case color-key
    "white" -2
    "black" -1
    "red" 0
    "orange" 30
    "yellow" 60
    "chartreuse" 90
    "green" 120
    "teal" 150
    "cyan" 180
    "sky-blue" 210
    "blue" 240
    "purple" 270
    "magenta" 300
    "fuchsia" 330
    -1)
   (case color-key
     :white -2
     :black -1
     :red 0
     :orange 30
     :yellow 60
     :chartreuse 90
     :green 120
     :teal 150
     :cyan 180
     :sky-blue 210
     :blue 240
     :purple 270
     :magenta 300
     :fuchsia 330
     -1)))

;;; Palettes

(defn hue->rgb [hue]
  (let [h #?(:clj (mod hue 360.0)
             :cljs (js-mod hue 360.0))
        c 255
        x #?(:clj (int (* 255 (- 1 (util/abs (- (mod (/ h 60.0) 2) 1)))))
             :cljs (int (* 255 (- 1 (util/abs (- (js-mod (/ h 60.0) 2) 1))))))]
    (condp > hue
      60  [c x 0]
      120 [x c 0]
      180 [0 c x]
      240 [0 x c]
      300 [x 0 c]
      360 [c 0 x])))

(defn- hsl->rgb [h s l]
  (let [c (* s (- 1 (util/abs (- (* 2 l) 1))))
        x (* c (- 1 (util/abs (- #?(:clj (mod (/ h 60.0) 2) :cljs (js-mod (/ h 60) 2)) 1))))
        m (- l (/ c 2))
        rgb (condp > h
              60  [c x 0]
              120 [x c 0]
              180 [0 c x]
              240 [0 x c]
              300 [x 0 c]
              360 [c 0 x]
              )]
    (mapv #(util/round (* (+ % m) 255)) rgb)))

(defn- derive-palette [r g b invert]
  (let [hx (rgb->hue r g b)
        hy #?(:clj (mod (+ hx 320) 360)
              :cljs (js-mod (+ hx 320) 360))
        hz #?(:clj (mod (+ (if invert hy hx) 180) 360)
              :cljs (js-mod (+ (if invert hy hx) 180) 360))
        c1 (hsl->rgb hx 1 0.1)
        c2 (hsl->rgb hx 1 0.2)
        c3 (hsl->rgb hx 1 0.45)
        c4 (hsl->rgb hx 1 0.7)
        c5 (hsl->rgb hy 1 0.8)
        c6 (hsl->rgb hz 1 0.45)
        c7 (hsl->rgb hz 1 0.8)]
    (if invert
      [nil c1 c4 c5 c2 c3 c6 c7]
      [nil c1 c2 c3 c4 c5 c6 c7])))

(defn cat-id->palette-helper [cat-id]
  (let [cat-id (util/clean-hex-prefix cat-id)
        genesis (str/starts-with? cat-id "f")
        k (util/hex->int (subs cat-id 2 4))
        r (util/hex->int (subs cat-id 4 6))
        g (util/hex->int (subs cat-id 6 8))
        b (util/hex->int (subs cat-id 8 10))
        invert (>= k 128)]
    (if genesis
      (if (or (and (even? k) invert) (and (odd? k) (not invert)))
        [nil [85 85 85] [211 211 211] [255 255 255] [170 170 170] [255 153 153] [17 17 17] [34 34 34]]
        [nil [85 85 85] [34 34 34] [17 17 17] [187 187 187] [255 153 153] [211 211 211] [255 255 255]])
      (derive-palette r g b invert))))

#?(:clj
   (def rescue-order->palette (embed "mooncatdata/extendedPaletteByRescueOrder.edn"))
   :cljs
   (defn rescue-order->palette [rescue-order]
     (cat-id->palette-helper (rescue-order->cat-id rescue-order))))

(defn cat-id->palette [cat-id]
  #?(:clj
     (if-let [rescue-order (get-rescue-order cat-id)]
       (rescue-order->palette rescue-order)
       (cat-id->palette-helper cat-id))
     :cljs
     (cat-id->palette-helper cat-id)))

;;; Palette Keys

(defn cat-id->palette-key-helper [cat-id]
  (let [cat-id (util/clean-hex-prefix cat-id)
        genesis (str/starts-with? cat-id "f")
        k (util/hex->int (subs cat-id 2 4))
        r (util/hex->int (subs cat-id 4 6))
        g (util/hex->int (subs cat-id 6 8))
        b (util/hex->int (subs cat-id 8 10))
        invert (>= k 128)]
    (if genesis
      (if (or (and (even? k) invert) (and (odd? k) (not invert)))
        "555555d3d3d3ffffffaaaaaaff9999"
        "555555222222111111bbbbbbff9999")
      (let [k-bin (util/byte->bin k)
            is-pure (= "00" (subs k-bin 4 6))
            palette (subvec (derive-palette r g b invert) 1 6)
            palette (if is-pure
                      (assoc palette 1 [0 0 0])
                      palette)]
        (str/join (map (fn [[r g b]] (str (util/byte->hex r) (util/byte->hex g) (util/byte->hex b))) palette))))))

#?(:clj
   (def rescue-order->palette-key (embed "mooncatdata/paletteKeyByRescueOrder.edn"))
   :cljs
   (defn rescue-order->palette-key [rescue-order]
     (cat-id->palette-key-helper (rescue-order->cat-id rescue-order))))

(defn cat-id->palette-key [cat-id]
  #?(:clj
     (if-let [rescue-order (get-rescue-order cat-id)]
       (rescue-order->palette-key rescue-order)
       (cat-id->palette-key-helper cat-id))
     :cljs
     (cat-id->palette-key-helper cat-id)))
