# libmooncat

[MoonCat​Community](https://mooncat.community) library for working with MoonCats in **clojure(script)**, and **javascript**.

This library provides utilities for:

 * MoonCat Information
 * MoonCat​Accessories
 * Image Generation (with and without accessories)
 * Ethereum RPC queries

Though it is a **Work in Progress**, it is currently used in many official MoonCat projects. Including the MoonCat metadata [API](//api.mooncat.community), the Accessory [Designer](//mooncat.community/accessory-designer), and the [Boutique](//boutique.mooncat.community).

## Javascript (browser)

see `dist-js/README.md`

## Javascript (node)

`@ponderware/libmooncat`

see `dist-node/README.md`

## Clojure

[![Clojars Project](https://img.shields.io/clojars/v/com.ponderware/libmooncat.svg)](https://clojars.org/com.ponderware/libmooncat)

[com.ponderware/libmooncat "1.1.4"]

### Usage

to generate the javascript library run `lein build-js-libs` (requires [leiningen](https://leiningen.org/))

to use in clojure(script) code, view doc-strings in `libmooncat.core`

#### shadow-cljs

To use in a shadow-cljs build, you will need to create a `cljsjs.pako` namespace on the classpath containing:

```clojure
(ns cljsjs.pako
  (:require ["pako" :as pako]))

(js/goog.exportSymbol "pako" pako)
```

---

## License

©2022 ponderware

AGPL v3
